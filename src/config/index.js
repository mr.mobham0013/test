import { api } from './api'

export default {
  appName: 'کالری شمار زیره',
  pageTitle: {
    min: 'زیره',
    full: 'کالری شمار زیره'
  },
  noneToken: ['home'],
  api,

  // the id of gram
  gramUnitId: 7,

  unitsPreValue: {
    4: 2, // قاشق غذا خوری
    5: 2, // حبه
    7: 100, // گرم
    11: 2, // کفگیر

    200: 30 //دقیقه
  }
}
