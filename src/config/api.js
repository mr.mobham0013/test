let isProduction = process.env.NODE_ENV === 'production'
const URL = isProduction
  ? 'http://panel.zirehapp.com/service/public/api'
  : 'http://test.zirehapp.com/service/public/api'

export const api = {
  baseUrl: URL,
  login: 'signup',
  verifyCode: 'verify'
}
