export default {
  install(Vue) {
    Vue.mixin({
      beforeCreate() {
        let options = this.$options
        if (!options.syncStore) return
        this.$options.computed = this.$options.computed || {}
        options.syncStore.forEach(item => {
          let [keyComponent, keyStore] = item.split(':')
          if (!keyStore) keyStore = keyComponent
          let keyStoreCap = keyStore.charAt(0).toUpperCase() + keyStore.slice(1)
          if (!options.computed.hasOwnProperty(keyComponent)) {
            this.$options.computed[keyComponent] = {
              set(value) {
                this.$store.commit(`update${keyStoreCap}`, value)
              },
              get() {
                return this.$store.state[keyStore]
              }
            }
          }
        })
      }
    })
  }
}
