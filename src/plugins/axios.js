import settings from '../modules/settings'
import config from '../config'
import store from '../store'
import { message } from './message'
import axiosInstance from 'axios'
import { camelKeys } from '../modules/utils'

const CancelToken = axiosInstance.CancelToken

/**
 * We need a vue instance to toggle the loading during request
 * or set some values like errors and authentications
 * @param vm VueInstance
 * @return {AxiosInstance}
 * @constructor
 */
const Axios = function(vm) {
  /**
   * The configuration setup for current request
   * this can change on `axios.setConfig({key: value})`
   * @cfg loader: to enable or disable the loading
   * @cfg showError: if true, an error message will show after error
   * @cfg authCheck: enable or disable authentication check
   * @cfg vm: the vue instance
   * @cfg vvm: the vue instance for validate
   */
  const options = {
    $vm: vm,
    $vvm: undefined,
    $loader: true,
    $showError: true,
    $authCheck: true,
    $validate: false,
    $scope: undefined,
    $loadingKey: 'loading',
    $mainLoading: false,
    $retry: false
  }

  /**
   * Show and hide loading during request
   * @param val Boolean
   */
  const loading = val => {
    if (
      options.$loader &&
      options.$vm &&
      typeof options.$vm[options.$loadingKey] === 'boolean'
    ) {
      options.$vm[options.$loadingKey] = val
    }
    if (options.$mainLoading) {
      store.commit('updateLoading', val)
    }
  }

  const Axios = axiosInstance.create({
    baseURL: config.api.baseUrl,
    headers: {
      'Content-Type': 'application/json'
    }
  })

  Axios.interceptors.request.use(
    /**
     * handle request before send
     * @param config
     * @returns {AxiosRequestConfig}
     */
    async function(config) {
      const token = settings.get('token')
      let op = config.data || {}

      if (op.$data) config.data = op.$data

      if (['get'].indexOf(config.method) !== -1) op = config

      for (let k in options) if (op.hasOwnProperty(k)) options[k] = op[k]

      if (token) config.headers['token'] = token

      if (options.$validate || options.$scope || options.$vvm) {
        let vm = options.$vvm || options.$vm
        let valid = await vm.$validator.validateAll(options.$scope)
        if (!valid)
          return {
            ...config,
            cancelToken: new CancelToken(cancel =>
              cancel('لطفا  همه ی گزینه ها را کامل کنید')
            )
          }
      }

      loading(true)
      return config
    },
    /**
     * handle request failure before send
     * @param error
     * @returns {Promise.<*>}
     */
    function(error) {
      loading(false)
      return Promise.reject(error)
    }
  )

  Axios.interceptors.response.use(
    /**
     * after request response
     * the request was successful
     * and we can parse response
     * @param response
     * @returns {any}
     */
    function(response) {
      loading(false)
      let data = response.data.data || response.data
      if (typeof data === 'number' || typeof data === 'string') return data
      return camelKeys(data)
    },
    /**
     * after request response
     * the request failed
     * we can handle the error
     * @returns {Promise.<*>}
     */
    function(error) {
      // let message  = error.message;
      // let response = error.response;
      // let request  = error.request;
      // let config   = error.config;
      let data

      try {
        data = error.response.data
      } catch (er) {
        data = {}
      }

      if (error.response === undefined) error.response = {}

      const response = {
        status: 400,
        statusText: '',
        ...error.response
      }

      if (response.status === 401 || response.statusText === 'Unauthorized') {
        message.error('باید مجددا وارد حساب کاربری شوید')
        settings.clear('token')
        options.$showError = false
      }

      let errorMessage =
        data.error ||
        data.message ||
        error.response.statusText ||
        error.message ||
        'لطفا دوباره تلاش کنید!'

      if (options.$validate) {
        try {
          let vm = options.$vm
          let errors = []
          if (data.errors) {
            for (let k in data.errors) errors = errors.concat(data.errors[k])
          }
          if (vm.errorList === undefined) {
            if (!errors.length) errors.push(errorMessage)
            errors.forEach(er => {
              message.add({
                type: 'danger',
                text: er,
                t: 30000,
                replace: er
              })
            })
          } else {
            vm.errorList = errors
            if (data.error) message.error(errorMessage)
          }
        } catch (e) {
          message.error(errorMessage)
        }
      } else if (options.$showError) {
        message.error(errorMessage)
      }

      // @todo:
      // here we will setup the retry popup

      if (options.$retry) {
        console.log('RETRY REQUEST::', options)
      }

      loading(false)
      return Promise.reject(error)
    }
  )

  /**
   * This function change the vue instance
   * sometimes we need to request at current component but change loading of other component
   * for example pass a modal component as vue instance `this.axios.bind(this.$refs.modal).get('...')`
   * => will result: modal.loading = true ...
   * @param vueInstance
   * @returns {AxiosInstance}
   */
  Axios.bind = vueInstance => {
    options.$vm = vueInstance
    return Axios
  }

  /**
   *
   * Change the default options
   * @example `this.axios.setConfig({$showError: false}).get('somewhere')`
   * @param newConfig
   * @returns {AxiosInstance}
   *
   */
  Axios.setConfig = newConfig => {
    for (let k in newConfig) {
      options[k] = newConfig[k]
    }
    return Axios
  }

  /**
   * Validate the form before submit request
   * @param scope
   * @param vm vue model
   * @returns {AxiosInstance}
   */
  Axios.validate = (vm, scope) => {
    options.$validate = true
    options.$scope = scope
    options.$vvm = vm
    return Axios
  }

  return Axios
}

const ex = new Axios()
export { Axios, ex as axios }

export default {
  install(Vue) {
    Object.defineProperties(Vue.prototype, {
      axios: {
        get() {
          return Axios(this)
        }
      }
    })
  }
}
