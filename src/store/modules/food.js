import { Axios } from '../../plugins/axios'
import moment from '../../plugins/moment'
import Vue from 'vue'
// import settings from '../../modules/settings'

const getMealByFoodId = (dayFoods, id) => {
  for (let date in dayFoods) {
    let meals = dayFoods[date]
    for (let m = 0, ml = meals.length; m < ml; m++) {
      let foods = meals[m].foods
      for (let i = 0, ii = foods.length; i < ii; i++) {
        if (foods[i].id === id) {
          return meals[m]
        }
      }
    }
  }
  return null
}

const getFoodById = (dayFoods, id) => {
  let meal = getMealByFoodId(dayFoods, id) || {}
  let foods = meal.foods || []
  for (let i = 0, ii = foods.length; i < ii; i++) {
    if (foods[i].id === id) {
      return foods[i]
    }
  }
  return null
}

const state = {
  meals: {
    breakfast: { icon: 'ic-coffee', isExtra: false },
    snack: { icon: 'ic-drink-glass', isExtra: false },
    lunch: { icon: 'ic-food-1', isExtra: false },
    evening: { icon: 'ic-cake', isExtra: false },
    dinner: { icon: 'ic-soup-1', isExtra: false },
    iftar: { icon: 'ic-soup-1', isExtra: true },
    matutinal: { icon: 'ic-food-1', isExtra: true },
    free: { icon: 'ic-apple', isExtra: true }
  },
  dayFoods: {},
  foods: [],
  favoriteFoods: []
}

const getters = {
  dayFoods(state) {
    return date => {
      let meals = state.dayFoods[date]
      if (!meals) return []
      meals.map(meal => {
        meal.totalCalories = meal.foods
          .map(f => f.calorie)
          .reduce((a, b) => a + b, 0)
        return meal
      })
      return meals
    }
  },
  favoriteFoods(state) {
    return state.foods.filter(f => state.favoriteFoods.includes(f.id))
  }
}

const mutations = {
  updateFoods(state, payload) {
    // state.foods = payload
    Vue.set(state, 'foods', payload)
  },
  updateDayFoods(state, payload) {
    Vue.set(state.dayFoods, payload.date, payload.data)
  },
  deleteMealFood(state, id) {
    let meal = getMealByFoodId(state.dayFoods, id) || {}
    let foods = meal.foods || []
    for (let i = 0, ii = foods.length; i < ii; i++) {
      if (foods[i].id === id) {
        foods.splice(i, 1)
        return true
      }
    }
    return false
  },
  addTemporaryFood(state, payload) {
    let exists = null
    let food = state.foods.find(f => f.id === payload.foodId)
    let meals = state.dayFoods[payload.date] || []
    let meal = meals.find(m => m.id === payload.mealId) || {}
    let foods = meal.foods || []

    let newFood = {
      // the Temporary filed is important
      temp: true,
      amount: payload.unitValue,
      calorie: payload.calorie,
      date: payload.date,
      foodId: payload.foodId,
      foodTitle: food.title,
      id: new Date().getTime(),
      image: food.img,
      mealId: payload.mealId,
      unitId: payload.unitId,
      unitTitle: payload.unitTitle
    }

    for (let i = 0, ii = foods.length; i < ii; i++) {
      if (foods[i].foodId === payload.foodId) {
        exists = i
        break
      }
    }

    // if not exists food in this meal so we push a new food
    // to meal and it will be saved as soon as the product is created
    if (exists === null) return meal.foods.push(newFood)

    // if food already exists in this meal?
    // so check if existing food is different or same
    // if is same, then we ignore it
    if (
      newFood.amount !== foods[exists].amount ||
      newFood.unitId !== foods[exists].unitId
    ) {
      Vue.set(foods, exists, newFood)
    }
  },
  updateFoodSaveStatus(state, payload) {
    let food = getFoodById(state.dayFoods, payload.id)
    Vue.set(food, 'status', payload.status)
  },
  updateFoodId(state, payload) {
    let food = getFoodById(state.dayFoods, payload.oldId)
    food.id = payload.newId
    Vue.delete(food, 'status')
    Vue.delete(food, 'temp')
  },
  updateFavoriteFoods(state, payload) {
    state.favoriteFoods = payload
    state.foods.forEach(food => {
      food.isFavorite = payload.includes(food.id)
    })
  },
  reset(state) {
    state.dayFoods = {}
    state.favoriteFoods = []
  }
}

const actions = {
  getDayFoods({ commit, state }, payload) {
    payload = {
      $data: {
        date: moment().format('YYYY-MM-DD')
      },
      ...payload
    }

    return new Axios().post('plan/food/date', payload).then(response => {
      let meals = response.map(meal => {
        meal.$data = state.meals[meal.type]
        return meal
      })
      commit('updateDayFoods', {
        date: payload.$data.date,
        data: meals
      })
      return meals
    })
  },
  addFood({ commit }, payload) {
    return new Axios()
      .post('plan/food', payload)
      .then(response => {
        commit('updateFoodId', {
          oldId: payload.$data.oldId,
          newId: response
        })
        return response
      })
      .catch(() => {
        commit('updateFoodSaveStatus', {
          id: payload.$data.oldId,
          status: 'fail'
        })
      })
  },
  deleteFood({ commit }, payload) {
    return new Axios().post('plan/delete/food', payload).then(() => {
      let id = payload.$data.id
      commit('deleteMealFood', id)
      return true
    })
  },
  getAllFoods({ state, commit }, payload) {
    return new Axios().get('food/tiny', payload).then(response => {
      let foods = response.map(food => {
        return {
          id: food.id,
          title: food.title,
          units: food.unitGram,
          tags: food.tag,
          category: food.category,
          calorie: Math.round(food.facts.calory * 100) / 100,
          img: food.img,
          nature: food.nature,
          rangeCalorie: food.rangeCalorie,
          rife: food.rife,
          search: food.title + ' ' + food.tag.map(t => t.title).join(' '),
          isFavorite: state.favoriteFoods.includes(food.id)
        }
      })
      commit('updateFoods', foods)
      return true
    })
  },
  getFavoriteFoods({ commit }, payload) {
    return new Axios().get('fav/food', payload).then(response => {
      commit('updateFavoriteFoods', response.map(food => food.id))
      return true
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
