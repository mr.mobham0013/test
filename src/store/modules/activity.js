import { Axios } from '../../plugins/axios'
import moment from '../../plugins/moment'
import Vue from 'vue'

const state = {
  dayActivities: [],
  activities: [],
  favoriteActivities: [],
  categories: []
}

const getters = {
  activities(state) {
    let result = {}
    state.dayActivities.forEach(item => {
      if (result[item.date] === undefined) {
        Vue.set(result, item.date, [])
      }
      result[item.date].push(item)
    })
    return result
  },
  dayActivities(state) {
    return date => state.dayActivities[date] || []
  },
  favoriteActivities(state) {
    return state.activities.filter(a => state.favoriteActivities.includes(a.id))
  }
}

const mutations = {
  updateActivities(state, payload) {
    Vue.set(state, 'activities', payload)
  },
  updateCategories(state, payload) {
    Vue.set(state, 'categories', payload)
  },
  updateDayActivities(state, payload) {
    // remove items that already exists in state
    let activities = state.dayActivities,
      length = activities.length
    payload.forEach(item => {
      if (!item) return
      for (let i = 0; i < length; i++) {
        let activity = activities[i]
        if (!activity) continue
        if (
          activity.activityId === item.activityId &&
          activity.date === item.date
        ) {
          activities.splice(i, 1)
        }
      }
    })
    payload.forEach(item => {
      state.dayActivities.push(item)
    })
  },
  deleteActivity(state, id) {
    let activities = state.dayActivities
    for (let i = 0, ii = activities.length; i < ii; i++) {
      if (activities[i].id === id) {
        activities.splice(i, 1)
        return true
      }
    }
    return false
  },
  addTemporaryActivity(state, payload) {
    let exists = null
    let activity = state.activities.find(a => a.id === payload.activityId)
    let activities = state.dayActivities

    let newActivity = {
      // the Temporary filed is important
      temp: true,
      amount: payload.amount,
      calorie: payload.calorie,
      date: payload.date,
      activityId: payload.activityId,
      activityTitle: activity.title,
      id: new Date().getTime(),
      image: activity.img
    }

    for (let i = 0, ii = activities.length; i < ii; i++) {
      if (
        activities[i].activityId === payload.activityId &&
        activities[i].date === payload.date
      ) {
        exists = i
        break
      }
    }

    // if not exists activity in this day so we push a new activity
    // to day and it will be saved as soon as the activity component is created
    if (exists === null) return activities.push(newActivity)

    // if activity already exists in this day?
    // so check if existing activity is different or same
    // if is same, then we ignore it
    if (newActivity.amount !== activities[exists].amount) {
      Vue.set(activities, exists, newActivity)
    }
  },
  updateActivitySaveStatus(state, payload) {
    let activity = state.dayActivities.find(i => i.id === payload.id)
    Vue.set(activity, 'status', payload.status)
  },
  // updateActivityId(state, payload) {
  //   let activity = state.dayActivities.find(i => i.id === payload.oldId)
  //   activity.id = payload.newId
  //   Vue.delete(activity, 'status')
  //   Vue.delete(activity, 'temp')
  // },
  updateFavoriteActivities(state, payload) {
    state.favoriteActivities = payload
    state.activities.forEach(activity => {
      activity.isFavorite = payload.includes(activity.id)
    })
  },
  reset(state) {
    state.dayActivities = []
    state.favoriteActivities = []
  }
}

const actions = {
  getDayActivities({ commit }, payload) {
    payload = {
      $data: {
        date: moment().format('YYYY-MM-DD')
      },
      ...payload
    }
    return new Axios().post('plan/action/date', payload).then(response => {
      let actions = Array.isArray(response) ? response : []
      let activities = []
      actions.forEach(item => {
        item.action = item.action || {}
        activities.push({
          id: item.id,
          activityId: item.action.id,
          activityTitle: item.action.title,
          amount: item.amount,
          date: item.date,
          image: item.action.image,
          calorie: Math.round((item.action.calorieBurn / 30) * item.amount)
        })
      })
      commit('updateDayActivities', activities)
      return activities
    })
  },
  addActivity({ commit }, payload) {
    return new Axios().post('plan/action', payload).catch(() => {
      commit('updateActivitySaveStatus', {
        id: payload.$data.oldId,
        status: 'fail'
      })
    })
  },
  deleteActivity({ commit }, payload) {
    return new Axios().post('plan/delete/action', payload).then(() => {
      let id = payload.$data.id
      commit('deleteActivity', id)
      return true
    })
  },
  getAllActivities({ state, commit }, payload) {
    return new Axios().get('fitamin/category', payload).then(response => {
      let categories = []
      let activities = []
      let actions = response || []
      actions.forEach(category => {
        categories.push({
          id: category.id,
          title: category.title,
          img: category.image1,
          img2: category.image2
        })
        category.actions.forEach(action => {
          activities.push({
            id: action.id,
            category: action.category,
            title: action.title,
            img: action.image,
            description: action.description,
            calorie: Math.round((action.calorieBurn / 30) * 100) / 100,
            rife: action.rife,
            rangeCalorie: action.rangeCalorie,
            popular: action.popular,
            search: action.title,
            isFavorite: state.favoriteActivities.includes(action.id)
          })
        })
      })
      commit('updateActivities', activities)
      commit('updateCategories', categories)
      return true
    })
  },
  getFavoriteActivities({ commit }, payload) {
    return new Axios().get('fav/action', payload).then(response => {
      commit('updateFavoriteActivities', response.map(activity => activity.id))
      return true
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
