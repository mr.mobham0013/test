import { Axios } from '../../plugins/axios'
import moment from '../../plugins/moment'
// import settings from '../../modules/settings'

const state = {
  plans: [],
  weightHistory: []
}

const getters = {
  hasData: state => date => {
    return state.plans.filter(plan => plan.date === date).length
  },
  plans(state) {
    let plan = {}
    state.plans.forEach(day => (plan[day.date] = day))
    return plan
  }
}

const mutations = {
  updatePlans(state, payload) {
    // remove plans that already exists in state
    payload.forEach(plan => {
      for (let i = 0, ii = state.plans.length; i < ii; i++) {
        if (state.plans[i].date === plan.date) {
          state.plans.splice(i, 1)
          break
        }
      }
    })
    // then push new plans to state
    payload.forEach(plan => {
      state.plans.push(plan)
    })
  },
  updateWeightHistory(state, payload) {
    state.weightHistory = payload
  },
  // this removes a day data from plans
  resetDay(state, date) {
    for (let i = 0, ii = state.plans.length; i < ii; i++) {
      if (state.plans[i].date === date) {
        state.plans.splice(i, 1)
        break
      }
    }
  },
  reset(state) {
    state.plans = []
    state.weightHistory = []
  }
}

const actions = {
  getDashboard({ commit, getters, state }, payload) {
    payload = {
      $force: false,
      $mainLoading: true,
      $data: {
        date: moment().format('YYYY-MM-DD')
      },
      ...payload
    }

    if (getters.plans[payload.$data.date] && !payload.$force)
      return Promise.resolve(state)

    return new Axios().post('dashboard', payload).then(response => {
      //let plans = response.weekPlan.filter(plan => !getters.plans[plan.date])
      let plans = response.weekPlan
      commit('updatePlans', plans)
      commit('updateWeightHistory', response.weightHistory)
      return state
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
