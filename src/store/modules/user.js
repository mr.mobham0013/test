import { Axios } from '../../plugins/axios'
import settings from '../../modules/settings'

const defaultUserData = {
  id: null,
  name: null,
  avatar: null,
  email: null,
  mobile: null,
  family: null,
  emailActive: false,
  mobileActive: false,
  activityId: null,
  birthYear: 1990,
  city: null,
  country: null,
  registeredAt: null,
  genderId: 1,
  goalCalorie: null,
  goalWeight: null,
  height: null,
  weight: null,
  wrist: null
}

const state = {
  data: {
    ...defaultUserData
  }
}

const getters = {
  hasData(state) {
    return typeof state.data.id === 'number'
  },
  hasGoalWeight() {
    return typeof state.data.goalWeight === 'number'
  }
}

const mutations = {
  updateData(state, payload) {
    state.data = payload
  },
  reset(state) {
    state.data = { ...defaultUserData }
  }
}

const actions = {
  signUp({ commit }, payload) {
    return new Axios().post('signup', payload).then(() => {
      commit('updateLoginState', 'verify', { root: true })
    })
  },
  verifyCode({ commit }, payload) {
    return new Axios().post('verify', payload).then(response => {
      settings.set('token', response.token)
      commit('updateLoginState', 'ok', { root: true })
    })
  },
  verifyEmailOrMobile(context, payload) {
    return new Axios()[payload._method](`verify/${payload._item}`, payload)
  },
  logout({ commit }, payload) {
    return new Axios().get('logout', payload).then(() => {
      settings.clear('token')
      commit('reset')
      commit('reset', null, { root: true })
      commit('home/reset', null, { root: true })
      commit('food/reset', null, { root: true })
      commit('activity/reset', null, { root: true })
    })
  },
  getProfile({ commit, getters, state }, payload) {
    payload = {
      $force: false,
      $mainLoading: true,
      $retry: true,
      ...payload
    }

    if (getters.hasData && !payload.$force) return Promise.resolve(state.data)

    return new Axios().get('profile', payload).then(response => {
      response = response || {}
      let info = response.info || {}
      let data = {
        id: response.id,
        name: response.name,
        avatar: response.avatar,
        email: response.email,
        mobile: response.mobile,
        family: response.family,
        emailActive: !!response.flgEmailActive,
        mobileActive: !!response.flgMobileActive,
        activityId: info.activity,
        birthYear: info.birthYear,
        city: info.city,
        country: info.country,
        registeredAt: info.createdAt,
        genderId: info.genderId,
        goalCalorie: info.goalCalorie,
        goalWeight: info.goalWeight,
        height: info.length,
        weight: info.weight,
        wrist: info.wrest
      }
      commit('updateData', data)
      return data
    })
  },
  updateProfile(context, payload) {
    return new Axios().put('profile', {
      $mainLoading: false,
      ...payload
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
