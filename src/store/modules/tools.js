import { Axios } from '../../plugins/axios'

const state = {}

const getters = {}

const mutations = {}

const actions = {
  getBMI(ctx, payload) {
    return new Axios().post('tool/diet', payload)
  },
  weighing(ctx, payload) {
    return new Axios().post('plan/weight', payload)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
