import Vue from 'vue'
import Vuex from 'vuex'

import state from './state'
import mutations from './mutations'
import actions from './actions'

// modules
import user from './modules/user'
import home from './modules/home'
import tools from './modules/tools'
import food from './modules/food'
import activity from './modules/activity'

Vue.use(Vuex)

export default new Vuex.Store({
  state,
  mutations,
  actions,
  modules: {
    user,
    home,
    tools,
    food,
    activity
  }
})
