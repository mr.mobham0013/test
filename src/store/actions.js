export default {
  async loadBasicData({ dispatch }, payload) {
    payload = {
      $force: false,
      $mainLoading: true,
      ...payload
    }

    //let [userData, dashboard] =
    await Promise.all([
      dispatch('user/getProfile', payload)
      //dispatch('home/getDashboard', payload)
    ])

    return new Promise((resolve, reject) => {
      resolve()
    })
  }
}
