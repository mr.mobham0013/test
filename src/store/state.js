export default {
  /**
   * main loading state
   */
  loading: false,
  /**
   * whether to show or hide sidebar
   * in phone size
   */
  showSidebar: false,

  /**
   * state for login
   * whether to show or hide login popup
   * state = login => will show "login section"
   * state = verify => will show "verify section"
   */
  showLogin: false,
  loginState: 'login',

  /**
   * whether to show or hide retry popup
   * this will show on http request failure
   */
  retry: {
    show: false,
    callback: null,
    message: ''
  }
}
