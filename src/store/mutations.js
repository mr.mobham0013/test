export default {
  updateLoading(state, value) {
    if (value) {
      state.loading++
    } else {
      setTimeout(() => {
        state.loading--
        if (state.loading < 0) state.loading = 0
      }, 100)
    }
  },
  updateShowSidebar(state, value) {
    state.showSidebar = !!value
  },
  updateShowLogin(state, value) {
    state.showLogin = value
  },
  updateLoginState(state, value) {
    state.loginState = value
  },
  updateRetry(state, value) {
    let type = typeof value
    if (type === 'boolean') {
      state.retry.show = value
    } else if (type === 'object') {
      for (let k in value) {
        if (value.hasOwnProperty(k) && state.retry.hasOwnProperty(k)) {
          state.retry[k] = value
        }
      }
    } else if (type === 'string') {
      state.retry.show = true
      state.retry.message = value
    } else if (type === 'function') {
      state.retry.show = true
      state.retry.callback = value
    }
  },
  reset(state) {
    state.showLogin = false
    state.loginState = 'login'
  }
}
