import Sidebar from './components/includes/Sidebar.vue'
import Navbar from './components/includes/Navbar.vue'
import Home from './components/home/Home.vue'
import store from './store'

const asyncRequire = view => () => {
  store.commit('updateLoading', true)
  const file = import(/* webpackChunkName: "view-[request]" */ `./components/${view}.vue`)
  file
    .then(() => {
      setTimeout(() => {
        store.commit('updateLoading', false)
      }, 400)
    })
    .catch((er) => {
      console.log({...er})
      window.location.reload()
    })
  return file
}

const cmp = component => ({
  default: component,
  sidebar: Sidebar,
  navbar: Navbar
})

export const routes = [
  {
    path: '/',
    name: 'home',
    components: cmp(Home),
    meta: { pageTitle: '' }
  },
  {
    path: '/profile',
    name: 'profile',
    components: cmp(asyncRequire('profile/Profile')),
    meta: { pageTitle: 'پروفایل', requiredAuth: true }
  },
  {
    path: '/target',
    name: 'target',
    components: cmp(asyncRequire('target/Target')),
    meta: { pageTitle: 'تعیین هدف', requiredAuth: true }
  },
  {
    path: '/activity',
    name: 'activity',
    components: cmp(asyncRequire('activity/Activity')),
    meta: { pageTitle: 'فعالیت ها' }
  },
  {
    path: '/food',
    name: 'food',
    components: cmp(asyncRequire('food/Food')),
    meta: { pageTitle: 'غذاها' }
  },
  {
    path: '/add-food',
    name: 'addFood',
    components: cmp(asyncRequire('addFood/AddFood')),
    meta: { pageTitle: 'افزودن خوراکی', requiredAuth: true }
  },
  {
    path: '/add-activity',
    name: 'addActivity',
    components: cmp(asyncRequire('addActivity/AddActivity')),
    meta: { pageTitle: 'افزودن فعالیت', requiredAuth: true }
  },
  {
    path: '/weighing',
    name: 'weighing',
    components: cmp(asyncRequire('weighing/Weighing')),
    meta: { pageTitle: 'اعلام وزن', requiredAuth: true }
  },
  {
    path: '/mag',
    name: 'mag',
    components: cmp(asyncRequire('mag/Mag')),
    meta: { pageTitle: 'مجله' }
  },
  {
    path: '/contact',
    name: 'contact',
    components: cmp(asyncRequire('pages/Contact')),
    meta: { pageTitle: 'ارتباط با زیره' }
  },
  {
    path: '/about',
    name: 'about',
    components: cmp(asyncRequire('pages/About')),
    meta: { pageTitle: 'درباره زیره' }
  },
  {
    path: '/exchange',
    name: 'exchange',
    components: cmp(asyncRequire('exchange/Exchange')),
    meta: { pageTitle: 'جایگزینی' }
  }
]
