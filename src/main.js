/**************************
 *        BASICS
 *************************/
import 'babel-polyfill'
import Vue from 'vue'
import App from './App.vue'
import store from './store'

/**************************
 *        STYLES
 *************************/
import './assets/custom-icon/zireh-icons.css'
import './assets/sass/_bootstrap.scss'
import './assets/sass/style.scss'

/**************************
 *        MODULES
 *************************/
import axios from './plugins/axios'
import { vueSettings } from './modules/settings'
import directives from './modules/directives'
import * as utils from './modules/utils'
import enums from './modules/enums'

/**************************
 *        PLUGINS
 *************************/
import config from './config'
import feather from 'vue-icon'
import modal from './plugins/modal'
import Fragment from 'vue-fragment'
import moment from './plugins/moment'
import vuexHelper from './plugins/vuexHelper'
import sticky from './modules/vue-sticky-directive'
import { vueMessage } from './plugins/message'
import { router, VueRouter } from './modules/router'
import { validationConfig, VeeValidate } from './modules/validate'
import VuePersianDatetimePicker from 'vue-persian-datetime-picker'

/**************************
 *        COMPONENTS
 *************************/
import AppHeader from './components/includes/AppHeader'
import Loader from './components/helper/Loader'
import CountTo from './components/helper/CountTo'
import TextScroll from './components/helper/TextScroll'

Vue.use(axios)
Vue.use(modal)
Vue.use(VueRouter)
Vue.use(vuexHelper)
Vue.use(vueMessage)
Vue.use(Fragment.Plugin)
Vue.use(feather, 'v-icon')
Vue.use(VeeValidate, validationConfig)
Vue.use(vueSettings)

Vue.use(utils.clone(VuePersianDatetimePicker), {
  name: 'date-picker',
  props: {
    format: 'YYYY-MM-DD',
    displayFormat: 'jDD jMMMM jYYYY',
    color: '#ff5981',
    autoSubmit: true
  }
})
Vue.use(utils.clone(VuePersianDatetimePicker), {
  name: 'time-picker',
  props: {
    type: 'time',
    format: 'HH:mm:00',
    altFormat: 'HH:mm:00',
    displayFormat: 'HH:mm',
    color: '#ff5981'
  }
})

Vue.util.defineReactive(Vue.prototype, '$config', config)
Vue.util.defineReactive(Vue.prototype, '$api', config.api)

Vue.component('Loader', Loader)
Vue.component('AppHeader', AppHeader)
Vue.component('CountTo', CountTo)
Vue.component('TextScroll', TextScroll)

Vue.directive('number', directives.number)
Vue.directive('focus', directives.focus)
Vue.directive('sticky', sticky)

Vue.prototype.$moment = moment
Vue.prototype.$enums = enums

/**************************
 *        THE APP
 **************************/
Vue.config.productionTip = false
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
