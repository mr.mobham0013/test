/**
 * converts array keys underscore to camelCase
 * @example ["foo_id" => 1] :: ["fooId" => 1]
 * @param o object or array
 * @return {any}
 */
function camelKeys(o) {
  let newO, origKey, newKey, value
  if (o instanceof Array) {
    return o.map(function(value) {
      if (typeof value === 'object') {
        value = camelKeys(value)
      }
      return value
    })
  } else {
    newO = {}
    for (origKey in o) {
      if (o.hasOwnProperty(origKey)) {
        newKey = origKey.replace(/_([a-z])/g, g => g[1].toUpperCase())
        value = o[origKey]
        if (
          value instanceof Array ||
          (value !== null && value.constructor === Object)
        ) {
          value = camelKeys(value)
        }
        newO[newKey] = value
      }
    }
  }
  return newO
}

function clone(obj) {
  let copy

  // Handle the 3 simple types, and null or undefined
  if (null == obj || 'object' != typeof obj) return obj

  // Handle Date
  if (obj instanceof Date) {
    copy = new Date()
    copy.setTime(obj.getTime())
    return copy
  }

  // Handle Array
  if (obj instanceof Array) {
    copy = []
    for (let i = 0, len = obj.length; i < len; i++) {
      copy[i] = clone(obj[i])
    }
    return copy
  }

  // Handle Object
  if (obj instanceof Object) {
    copy = {}
    for (let attr in obj) {
      if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr])
    }
    return copy
  }

  throw new Error("Unable to copy obj! Its type isn't supported.")
}

function rand(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min)
}

export { camelKeys, clone, rand }
