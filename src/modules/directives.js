/**
 * directive for number inout
 * <input type="tel" v-number="" .../>
 * with this directive, the value of input always is a valid number
 */
const number = {
  bind: function(el, binding) {
    el.addEventListener('input', function() {
      let value = el.value || ''
      let replaceArray = function(replaceString, find, replace) {
        let regex
        for (let i = 0; i < find.length; i++) {
          regex = new RegExp(find[i], 'g')
          replaceString = replaceString.replace(regex, replace[i])
        }
        return replaceString
      }

      // replace arabic and persian numbers

      let fa = '۰۱۲۳۴۵۶۷۸۹'.split('')
      let ar = '٠١٢٣٤٥٦٧٨٩'.split('')
      let en = '0123456789'.split('')

      value = replaceArray(value, fa, en)
      value = replaceArray(value, ar, en)

      if (binding.modifiers && binding.modifiers.numeric) {
        value = value.replace(/\D/g, '')
      }

      if (el.value !== value) {
        let start = this.selectionStart,
          end = this.selectionEnd
        el.value = value
        let event = new Event('input', {
          bubbles: true,
          cancelable: true
        })
        this.setSelectionRange(start, end)
        el.dispatchEvent(event)
      }
    })
  }
}

const focus = {
  inserted: function(el, binding) {
    if (binding.value !== false) el.focus()
  }
}

export default { number, focus }
