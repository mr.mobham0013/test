import VueCookie from 'vue-cookie'
import Vue from 'vue'
/**
 *
 * settings.set('token', 'abc')
 * settings.get()          => returns all settings = {...}
 * settings.get('token')   => returns "token" settings = 'abc'
 * settings.clear('token') => clear   "token"
 * settings.clear()        => clear all settings
 *
 * and so on ...
 *
 */

// const settings = {
//   key: '__ZIREH__LOCALE__CONFIG__',
//   watching: [],
//   data: {},
//   init() {
//     this.data = this.get() || {}
//   },
//   get: function(key) {
//     let data = '{}'
//
//     if (window.localStorage === undefined) {
//       data = VueCookie.get(this.key) || data
//     } else {
//       data = window.localStorage.getItem(this.key) || data
//     }
//
//     try {
//       data = JSON.parse(data)
//     } catch (er) {
//       // eslint-disable-next-line no-console
//       console.warn('Cannot parse settings: ', data)
//       data = {}
//     }
//
//     if (key) data = data[key]
//
//     return data
//   },
//   set: function(key, value) {
//     let data = this.get()
//
//     data[key] = value
//
//     data = JSON.stringify(data)
//
//     this.onChange(key, value)
//
//     if (window.localStorage === undefined) {
//       VueCookie.set(this.key, data, 100)
//     } else {
//       window.localStorage.setItem(this.key, data)
//     }
//   },
//   /**
//    * get or set if not exists
//    * @param key
//    * @param defaultValue
//    */
//   getOrSet(key, defaultValue) {
//     let result = this.get(key)
//     if (result === undefined) {
//       result = defaultValue
//       this.set(key, defaultValue)
//     }
//     return result
//   },
//   clear(key) {
//     let data = this.get()
//
//     this.onChange(key, undefined)
//
//     if (key) {
//       if (data[key] === undefined) return
//       data[key] = undefined
//     } else {
//       data = {}
//     }
//
//     data = JSON.stringify(data)
//
//     if (window.localStorage === undefined) {
//       VueCookie.set(this.key, data, 100)
//     } else {
//       window.localStorage.setItem(this.key, data)
//     }
//   },
//   onChange(key, value) {
//     this.watching.forEach(item => {
//       if (item.key === key) {
//         item.vm[key] = value
//       }
//     })
//   },
//   bind(vm, property, key, defaultValue) {
//     if (!defaultValue) defaultValue = vm[property]
//     vm[property] = this.getOrSet(key, defaultValue)
//     this.watching.push({ key, vm })
//     vm.$watch(property, val => {
//       this.set(key, val)
//     })
//   }
// }

const KEY = '__ZIREH__LOCALE__CONFIG__'

const DATA = {
  config: {}
}

const save = val => {
  let data = JSON.stringify(val)
  // console.log('SAVE:::', data)
  if (window.localStorage === undefined) {
    VueCookie.set(KEY, data, 100)
  } else {
    window.localStorage.setItem(KEY, data)
  }
}

const init = () => {
  let data = {}
  if (window.localStorage === undefined) {
    data = VueCookie.get(KEY) || data
  } else {
    data = window.localStorage.getItem(KEY) || data
  }
  try {
    data = JSON.parse(data)
  } catch (er) {
    data = {}
  }
  data = {
    ...DATA.config,
    ...data
  }
  Vue.set(DATA, 'config', data)
}

init()

const Settings = new Vue({
  data() {
    return {
      config: DATA.config
    }
  },
  watch: {
    config: {
      handler: save,
      deep: true
    }
  },
  methods: {
    set: function(key, value) {
      // console.log('SET', key, value)
      this.$set(this.config, key, value)
    },
    get: function(key) {
      return this.config[key]
    },
    clear(key) {
      // console.log('CLEAR', key)
      if (key) {
        this.config[key] = undefined
        //return save(this.config)
      } else {
        this.config = {}
      }
    }
  }
})

export default Settings

export const vueSettings = {
  install(Vue) {
    Vue.prototype.$settings = Settings

    const createComputed = function createComputed(key, defaultValue) {
      let [keyComponent, keyStorage] = key.split(':')
      if (!keyStorage) keyStorage = keyComponent
      this.$options.computed = this.$options.computed || {}
      if (
        Settings.config[keyStorage] === undefined &&
        defaultValue !== undefined
      ) {
        Settings.set(keyStorage, defaultValue)
      }
      if (!this.$options.computed.hasOwnProperty(keyComponent)) {
        this.$options.computed[keyComponent] = {
          set(value) {
            if (Settings.get(keyStorage) !== value)
              Settings.set(keyStorage, value)
          },
          get() {
            return Settings.config[keyStorage]
          }
        }
      }
    }

    Vue.mixin({
      beforeCreate: function VueSettingsOnCreate() {
        if (!this.$options.syncSettings) return
        this.$options.syncSettings.forEach(item => {
          if (typeof item === 'string') {
            createComputed.call(this, item)
          } else {
            for (let key in item) {
              if (item.hasOwnProperty(key))
                createComputed.call(this, key, item[key])
            }
          }
        })
      }
    })
  }
}
