import VueRouter from 'vue-router'
import config from '../config'
import { routes } from '../routes'
import settings from './settings'
import store from '../store'

const router = new VueRouter({
  routes: routes,
  mode: 'history',
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) return savedPosition
    if (to.hash) return { selector: to.hash }
    return { x: 0, y: 0 }
  }
})

router.beforeEach((to, from, next) => {
  document.title = to.meta.pageTitle
    ? config.pageTitle.min + ' | ' + to.meta.pageTitle
    : config.pageTitle.full

  store.commit('updateShowSidebar', false)
  window.dispatchEvent(new Event('resize'))

  let token = settings.get('token')
  let needToken = to.meta.requiredAuth

  if (!token && needToken) {
    return store.commit('updateShowLogin', true)
  }

  if (token) {
    return store
      .dispatch('loadBasicData')
      .then(() => next())
      .catch(() => {
        //console.log({ ...err })
        if (needToken && to.name !== 'home') {
          next({ name: 'home' })
        } else {
          next()
        }
      })
  }

  next()
})

export { router, VueRouter }
