import VeeValidate, { Validator } from 'vee-validate'
import fa from 'vee-validate/dist/locale/fa'

//Validator.addLocale(fa);
Validator.localize('fa', fa)

Validator.dictionary.setDateFormat('locale', 'YYYY/MM/DD')

Validator.extend('mobile', {
  getMessage: field => field + ' معتبر نیست.',
  validate: value => /^09[0-9]{9}$/.test(value)
})

Validator.extend('date', {
  getMessage: field => field + ' معتبر نیست.',
  validate: value =>
    /^13([0-9]{2})\/(0?[1-9]|1[0-2])\/(0?[1-9]|[12][0-9]|3[01])$/.test(value)
})

Validator.extend('datetime', {
  getMessage: field => field + ' معتبر نیست.',
  validate: value =>
    /^13([0-9]{2})\/(0?[1-9]|1[0-2])\/(0?[1-9]|[12][0-9]|3[01])((\s+)([0-9]{1,2}:[0-9]{1,2}(:[0-9]{1,2})?))?$/.test(
      value
    )
})

Validator.extend('email-or-mobile', {
  getMessage: field => field + ' معتبر نیست.',
  // eslint-disable
  validate: value =>
    value.replace(/[0-9]/gi, '') === ''
      ? /^09[0-9]{9}$/.test(value)
      : /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(
          value
        )
  // eslint-enable
})

const validationConfig = {
  errorBagName: 'errors',
  fieldsBagName: '$fields',
  delay: 100,
  locale: 'fa',
  dictionary: {
    fa: {
      messages: {
        required: () => 'اینو پرنکردی',
        min: (e, d) => ` حداقل ${d} حرف `,
        confirmed: field => field + ' با اصلش یکی نیست ',
        mobile: () => 'شماره موبایل باید با فرمت 09XXXXXXXXX باشد.',
        date: () => 'تاریخ باید با فرمت YYYY/MM/DD باشد.',
        datetime: () => 'تاریخ باید با فرمت YYYY/MM/DD HH:mm باشد.',
        length: () => 'طول رشته معتبر نیست'
      }
    }
  },
  strict: true,
  classes: true,
  classNames: {
    touched: 'touched',
    untouched: 'untouched',
    valid: 'valid',
    invalid: 'invalid',
    pristine: 'pristine',
    dirty: 'dirty'
  },
  events: 'input',
  inject: true
}

export { VeeValidate, validationConfig }
