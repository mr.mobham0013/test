var path = require('path')
var webpack = require('webpack')
var isProduction = process.env.NODE_ENV === 'production'

var fs = require('fs')
var version = new Date().getTime()

if (isProduction) {
  fs.readFile('./index.html', 'utf8', function(err, data) {
    if (err) {
      // eslint-disable-next-line
      return console.log(err)
    }

    var result = data.replace(
      new RegExp(/build\.js\?v=(.+?)"/g),
      'build.js?v=' + version + '"'
    )

    fs.writeFile('./index.html', result, 'utf8', function(err) {
      // eslint-disable-next-line
      if (err) return console.log(err)
    })
  })
}

module.exports = {
  entry: './src/main.js',
  output: {
    path: path.resolve(__dirname, './dist'),
    publicPath: 'dist/',
    filename: 'build.js?v=' + version
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            // Since sass-loader (weirdly) has SCSS as its default parse mode, we map
            // the "scss" and "sass" values for the lang attribute to the right configs here.
            // other preprocessors should work out of the box, no loader config like this necessary.
            scss: 'vue-style-loader!css-loader!sass-loader',
            sass: 'vue-style-loader!css-loader!sass-loader?indentedSyntax'
          }
          // other vue-loader options go here
        }
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: 'images/[name].[ext]?[hash]'
        }
      },
      {
        test: /\.(ttf|woff|eot|woff2)$/,
        loader: 'file-loader',
        options: {
          name: 'fonts/[name].[ext]?[hash]'
        }
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.scss$/,
        use: [
          { loader: 'vue-style-loader' },
          { loader: 'css-loader', options: { sourceMap: !isProduction } },
          'postcss-loader',
          { loader: 'sass-loader', options: { sourceMap: !isProduction } }
        ]
      }
    ]
  },
  resolve: {
    extensions: ['.vue', '.js'],
    alias: {
      '~': path.resolve(__dirname, './src/assets')
    }
  },
  plugins: [new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/)],
  performance: {
    hints: false
  },
  devServer: {
    historyApiFallback: true,
    inline: true,
    port: 9393
  },
  devtool: '#eval-source-map'
}

if (isProduction) {
  module.exports.devtool = '#source-map'
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      //sourceMap: true,
      sourceMap: false,
      compress: {
        warnings: false
      }
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true
    })
  ])
}
